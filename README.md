[![pipeline status](https://gitlab.com/uilianries/test-conan-promote/badges/master/pipeline.svg)](https://gitlab.com/uilianries/test-conan-promote/commits/master)

# Test Conan Promote

A test to validate Conan promote package

### LICENSE
[MIT](LICENSE.md)
